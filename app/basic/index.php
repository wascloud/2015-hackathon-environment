<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>TELUS Hackathon</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		
		<!-- BASIC CSS -->
    <link rel="stylesheet" type="text/css" href="//static.telus.com/my-account/217/assets/css/uss-framework/framework.css?1432847274">    
    <link rel="stylesheet" href="//static.telus.com/common/css/sandboxes/components/grids/1.1.0/grid.css">		

    <!-- BASIC JS -->
    <script type="text/javascript"  src="//static.telus.com/my-account/217/assets/js/libs/modernizr.js?1427265057"></script>

		<link rel="stylesheet" href="dist/css/main.css">
	</head>
	<body>
		<?php include 'includes/header.php'; ?>
		
		<div class="m-box">
			<h1>TELUS Hackathon</h1>

			<?php
				echo '<p>Your PHP Server is running!</p>' . "\n";
				echo '<p>Change the border color in <code>/app/basic/src/scss/main.css</code> to see if BrowserSync is working.</p>' . "\n";
			?>
		</div>

		<!-- BASIC JS -->
  	<script type="text/javascript" src="//static.telus.com/my-account/217/assets/js/libs/require.js"></script>
    <script src="dist/js/conf/require.conf.js"></script> 

    <!-- PAGE JS -->
		<script src="dist/js/test.js"></script>    
	</body>
</html>
