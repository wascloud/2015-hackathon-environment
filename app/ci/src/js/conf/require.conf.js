/**
 * RequireJS configurations file.
 * Also being used by JS build and unit tests.
 */

requirejs.config({
  baseUrl: '//static.telus.com/my-account/assets/js',
  paths: {
    //libs
    'libs': 'libs',
    //detail libs
    'jquery': 'libs/jquery',
    'jquery-migrate': 'libs/jquery-migrate',
    'hammer': 'libs/hammer',
    'jquery.easing': 'libs/plugins/jquery.easing',
    'jquery.hammer': 'libs/plugins/jquery.hammer',
    'royalslider' : 'libs/plugins/jquery.royalslider.complete.min',
    'jquery.placeholder' : 'libs/plugins/jquery.placeholder.min',
    'jquery-ui' : 'libs/plugins/jquery-ui.min',
    'jquery-ui-autocomplete' : 'libs/plugins/jquery-ui-autocomplete',
    'jquery.select-to-autocomplete' : 'libs/plugins/jquery.select-to-autocomplete.modified',
    'jquery-address' :'libs/plugins/jquery-address',
    'jquery.formatter': 'libs/plugins/jquery.formatter',
    'handlebars' : 'libs/handlebars',

    //angular stuff
    'angular': 'libs/angular',
    'angular-animate': 'libs/angular-plugins/angular-animate',
    'angular-cookies': 'libs/angular-plugins/angular-cookies',
    'angular-touch': 'libs/angular-plugins/angular-touch',
    'angular-resource': 'libs/angular-plugins/angular-resource',
    'angular-aria': 'libs/angular-plugins/angular-aria',
    'angular-ui-router': 'libs/angular-plugins/angular-ui-router',
    'angular-local-storage': 'libs/angular-plugins/angular-local-storage',
    'angular-messages': 'libs/angular-plugins/angular-messages',
    'angular-templates': 'app/templates',
    //architect
    'architect': 'framework/architect',

    //utils
    'utils': 'framework/utils',

    //widgets
    'app-widgets': 'app/widgets',
    'framework-widgets': 'framework/widgets',

    //common
    'common': '/sandboxes/common'
  },
  shim: {
    'jquery': {
      exports: ['$','jQuery']
    },
    'jquery-migrate': {
      deps: ['jquery']
    },
    'jquery.easing': {
      deps: ['jquery']
    },
    'jquery.hammer': {
      deps: ['hammer','jquery']
    },
    'jquery.address': {
      deps: ['jquery']
    },
    'jquery.formatter': {
      deps: ['jquery']
    },
    'jquery.placeholder':{
      deps: ['jquery']
    },
    'jquery-ui':{
      deps: ['jquery']
    },
    'jquery-ui-autocomplete':{
      deps: ['jquery']
    },
    'jquery.select-to-autocomplete':{
      deps: ['jquery', 'jquery-migrate', 'jquery-ui-autocomplete']
    },
    'angular': {
      exports: 'angular'
    },
    'handlebars': {
      exports: 'Handlebars'
    },
    'angular-ui-router':{
      deps: ['angular']
    },
    'angular-resource':{
      deps: ['angular']
    },
    'angular-cookies':{
      deps: ['angular']
    },
    'angular-animate':{
      deps: ['angular']
    },
    'angular-touch':{
      deps: ['angular']
    },
    'angular-aria':{
      deps: ['angular']
    },
    'angular-local-storage':{
      deps: ['angular']
    },
    'angular-messages':{
      deps: ['angular']
    },
    'angular-templates':{
      deps: ['angular']
    }
    

  },
  // This will wrap shimmed dependencies in a define() call so that they work 
  // better after a build when their upstream dependencies are also AMD modules
  // with dependencies.
  wrapShim: 'true'
});
